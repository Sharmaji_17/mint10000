$(() => {
    // $("#found").hide();

    // $("#hide").click(() => {
    //     console.log("hide");
    //     $("#found").hide();
    // });

    const Token = localStorage.getItem("authorization");

    var table = $("#table_id").DataTable({
        lengthMenu: [
            [5, 10, 25, 1],
            [5, 10, 25, "All"],
        ],
        processing: true,
        serverSide: true,
        responsive: true,
        headers: {
            authorization: localStorage.getItem("authorization"),
            sign: sign.signature,
            hash: sign.hash,
        },
        ajax: {
            method: "POST",
            url: "/api/v1/user/render",
            data: {
                sAddress: sAccountAddress,
                sign: sign.signature,
                hash: sign.hash,
            },
        },
        columns: [{
                data: "_id",
            },
            {
                data: "sAddress",
            },
            {
                data: "nTimeStamp",
                render: function(data, type, row) {
                    return (
                        new Date(data).toLocaleDateString("IST") +
                        ", \n" +
                        new Date(data).toLocaleTimeString("en-US")
                    );
                },
            },
            {
                data: "operations",
                render() {
                    return '<input type="button"   class="update btn btn-primary" value="Show"> ';
                },
            },
        ],
    });

    // $("#table_id tbody").on("click", ".delete", function(e) {
    //     // console.log(e);
    //     // console.log(table.row(this).data());
    //     data = table.row($(this).parent().parent()).data();
    //     // console.log(parent());
    //     console.log(data);
    //     // console.log(Selected);

    //     let find = {
    //         email: data.email,
    //     };

    //     swal({
    //         title: `Delete ${data.name} ??`,
    //         text: "Once deleted, you will not be able to recover this imaginary file!",
    //         icon: "warning",
    //         buttons: true,
    //         dangerMode: true,
    //     }).then((willDelete) => {
    //         if (willDelete) {
    //             $.ajax({
    //                 type: "delete",
    //                 url: "/api/v1/admin/delete",
    //                 headers: { Authorization: Token },
    //                 data: find,

    //                 success: function(result, status, xhr) {
    //                     table.ajax.reload();
    //                     swal("Poof! Your imaginary file has been deleted!", {
    //                         icon: "success",
    //                     });
    //                     console.log(result);
    //                 },
    //                 error: function(error) {
    //                     console.log(error);
    //                     alert("error");
    //                 },
    //             });
    //         } else {
    //             swal("Your imaginary file is safe!");
    //         }
    //     });
    // });

    // $("#table_id").on("click", ".update", function() {
    //     data = table.row($(this).parent().parent()).data();

    //     let find = {
    //         email: data.email,
    //     };

    //     $.ajax({
    //         url: "/api/v1/admin/find",
    //         method: "post",
    //         data: find,
    //         headers: { Authorization: Token },
    //         success(result, status, xhr) {
    //             console.log("resyult: ", result);

    //             if (result.length == 0) {
    //                 swal({
    //                     title: "Error",
    //                     text: "Invalid Entrirs try Again",
    //                     icon: "error",
    //                     button: "Try Again",
    //                 });
    //             } else {
    //                 // $('#found').show();
    //                 $("#updateBTN").show();
    //                 $("#id").val(result._id);
    //                 $("#iname").val(result.name);
    //                 $("#iemail").val(result.email);
    //                 $("#found").show();
    //             }
    //         },
    //         error() {
    //             swal({
    //                 title: "Error",
    //                 text: "Something went wrong",
    //                 icon: "error",
    //                 button: "okey",
    //             });
    //         },
    //     });
    // });

    // $("#updateBTN").click(() => {
    //     let change = {
    //         _id: $("#id").val(),
    //         name: $("#iname").val(),
    //         email: $("#iemail").val(),
    //     };
    //     if (!change.name || !change.email) {
    //         console.log(change);
    //         swal({
    //             title: "Empty Field Somewere",
    //             text: "Please Enter valid Values in form",
    //             icon: "error",
    //             button: "okey",
    //         });
    //     } else {
    //         console.log(change);

    //         let token = localStorage.getItem("token");

    //         $.ajax({
    //             url: "api/v1/admin/update",
    //             method: "PUT",
    //             data: change,
    //             headers: {
    //                 authorization: token,
    //             },
    //             success(result, status, xhr) {
    //                 console.log(result);
    //                 swal({
    //                     title: "Success",
    //                     text: "Successfully Changed",
    //                     icon: "success",
    //                     button: "okey",
    //                 });
    //                 $("#found").hide();
    //                 table.ajax.reload();
    //             },
    //             error() {
    //                 swal({
    //                     title: "Empty Field",
    //                     text: "Something Went Wrong",
    //                     icon: "error",
    //                     button: "okey",
    //                 });
    //                 $("#found").hide();
    //             },
    //         });
    //     }
    // });
});