let sAccountAddress;
let mintCounter;
let sign = {};

$(document).ready(function() {
    // console.log("HEllooooooooooo");
    $("#ownable").hide();
    let contented = false;

    $("#ToTable").addClass("disabled");
    // =============================================================Installd Matamask?????===============
    const web3 = new Web3(window.ethereum);
    if (typeof window.ethereum !== "undefined") {
        console.log("Installed");
    } else {
        alert("Matamask is not installed you can't Access Any Functionaliity");
        console.log("Not Installed");
    }

    // let nAmount;
    let nTokenPrice;
    ethereum.on("accountsChanged", () => {
        connect();
    });

    //  ==========================Connec to Matamask==============================

    $("#disconnect").hide();
    $("#Wait").hide();
    async function connect() {
        const Fetch_sAccountAddress = await ethereum.request({
            method: "eth_requestAccounts",
        });
        console.log(Fetch_sAccountAddress[0]);
        sAccountAddress = `${Fetch_sAccountAddress[0]}`;
        ownable();

        $.ajax({
            url: "api/v1/user/onConnect",
            method: "post",
            data: { address: sAccountAddress },
            success(result, status, xhr) {
                $("#disconnect").show();
                $("#connect").hide();
                alert("Successfully Connected");
                // console.log("Database Data:", result);
                // console.log("Database Data:", JSON.parse(result.message));
                $("#ToTable").removeClass("disabled");
                contented = true;
            },
            error(xhr, status, error) {
                alert("Error");
                console.log(error);
            },
        });
        console.log(await ethereum.request({ method: "eth_chainId" }));
    }

    // ===================================Signature===========================================
    async function getSign() {
        let signature = await web3.eth.personal.sign(
            "Hello Welcome to this amazing app",
            sAccountAddress
        );

        let hash = await web3.eth.accounts.hashMessage(
            "Hello Welcome to this amazing app"
        );

        console.log(hash);
        sign = {
            signature,
            hash,
        };
        return sign;
    }

    var contract = new web3.eth.Contract(
        ABI,
        "0x3eCfba126f54De51f7429D6f599D233F3aB859bD"
    );
    // ==================================================On Load===============================
    async function ownable() {
        let deside = await onlyOwnner();
        console.log(deside);
        if (!deside) {
            // console.log(onlyOwnner());
            $("#ownable").hide();
            console.log("Hidden");
        } else {
            // console.log(onlyOwnner());
            $("#ownable").show();
            console.log("Not Hidden");
        }
    }

    ethereum.on("accountsChanged", (accounts) => {
        ownable();
    });
    // =========================================================Create Token-Session=======================
    async function logIn() {
        $.ajax({
            method: "post",
            url: "/api/v1/auth/signin",
            data: {
                sAddress: sAccountAddress,
                sign: sign.signature,
                hash: sign.hash,
            },
            success(result, status, xhr) {
                alert("Succesfully Connected");
                console.log(result);
                localStorage.setItem("authorization", result.token);
            },
            error(xhr, status, error) {
                console.log(error);
                alert("Something Went Worng");
            },
        });
    }

    // ==================================Set Price==========================================================

    async function setPrice() {
        // let sTxHash;
        let nEstimate;
        let nAmount = Number($("#price").val());
        let sUnit = $("#unit").val();
        nTokenPrice = nAmount;
        console.log(sUnit);
        $("#Wait").show();

        if (!nAmount || typeof nAmount != "number" || nAmount < 0) {
            console.log(typeof nAmount);
            console.log(nAmount);
            alert("invalid argument");
            $("#Wait").hide();

            return;
        }

        if (sUnit == "Eth" && nAmount >= 100) {
            alert("Upper Price Limit Try Lesser value");
            $("#Wait").hide();
            return;
        }

        if (sUnit == "Eth" && nAmount <= 100) {
            nAmount = await web3.utils.toWei(String(nAmount));
            console.log("---------------");
            console.log(nAmount);
            console.log("---------------");
        } else {
            console.log("---------------");
            nAmount = BigInt(nAmount);
            console.log(nAmount);
            console.log("---------------");
        }
        await contract.methods
            .setPrice(String(nAmount))
            .estimateGas({ from: sAccountAddress })
            .then(function(gasAmount) {
                nEstimate = Number(gasAmount);
                console.log(nEstimate);
                $("#Wait").hide();
            })
            .catch(function(error) {
                $("#Wait").hide();
                console.log(error);
            });
        console.log("Final Amount==>", nAmount);
        if (confirm(`gas Valuse is ${nEstimate}`)) {
            contract.methods
                .setPrice(String(nAmount))
                .send({ from: sAccountAddress })
                .on("transactionHash", (txHash) => {
                    sTxHash = txHash;
                    console.log("Transaction Hash: ", txHash);
                })
                // Getting transacton receipt
                .on("receipt", (txReceipt) => {
                    console.log("Transaction Reciept: ", txReceipt);
                    console.log("Total used Gas: ", txReceipt.gasUsed);
                })
                .then(() => {
                    console.log("Success");
                    priceCalculation();
                    alert("Price is Set");
                })

            .catch(function(error) {
                console.log(error);
            });

            console.log(nEstimate);
        } else {}
    }

    // =======================================Buy Token==============================================
    async function buyTokens() {
        let nEstimate;
        let nQty = Number($("#customRange3").val());
        let sTxHash;
        $("#Wait").show();
        // console.log(web3.utils.toWei(String(BigInt(nTokenPrice * nQty))));
        if (nQty > 10 || typeof nQty != "number" || nQty <= 0) {
            alert("invalid argument");
            return;
        }
        if (!localStorage.getItem("authorization")) {
            alert("Session Expired Connect Again");
            return;
        }
        nTokenPrice = await contract.methods
            .price()
            .call({ from: sAccountAddress });
        console.log("Final Buying Amount", nTokenPrice * nQty);
        await contract.methods
            .buyToken(Number(nQty))
            .estimateGas({
                from: sAccountAddress,
                value: nTokenPrice * nQty,
            })
            .then(function(gasAmount) {
                nEstimate = Number(gasAmount);
                // console.log(nEstimate);
                // console.log(gasAmount);
                $("#Wait").hide();

                // mintCounter += nQty;
            })
            .catch(function(error) {
                console.log(error);
            });
        console.log(nEstimate);

        if (confirm(`gas Valuse is ${nEstimate}`)) {
            console.log(String(nTokenPrice * nQty));
            contract.methods
                .buyToken(Number(nQty))
                .send({
                    from: sAccountAddress,
                    value: String(nTokenPrice * nQty),
                })
                .on("transactionHash", (txHash) => {
                    sTxHash = txHash;
                    console.log("Transaction Hash: ", txHash);
                })
                // Getting transacton receipt
                .on("receipt", (txReceipt) => {
                    console.log("Transaction Reciept: ", txReceipt);
                    console.log("Total used Gas: ", txReceipt.gasUsed);
                })
                .then(() => {
                    referesh();
                    console.log("Success");
                    $("#Wait").hide();
                    $.ajax({
                        url: "api/v1/user/onBuy",
                        method: "post",
                        headers: {
                            authorization: localStorage.getItem("authorization"),
                            sign: sign.signature,
                            hash: sign.hash,
                        },
                        data: {
                            sAddress: sAccountAddress,
                            sTxHash: sTxHash,
                            nQuantity: nQty,
                            nAmount: nTokenPrice * nQty,
                        },
                        success(result, status, xhr) {
                            alert("Successful Transection");
                            console.log("Data Saved");
                            $("#Wait").hide();
                            console.log(result);
                        },
                        error(xhr, status, error) {
                            alert("Error");
                            console.log(error);
                        },
                    });
                    web3.eth.getBlock("latest").then(console.log);
                })
                .catch((error) => {
                    console.log(error);
                    // rejected();
                });
        } else {
            console.log("Rejected");
        }
    }

    // .============================================Progress Bar=============================================================

    async function getBalance() {
        mintCounter = await contract.methods
            ._tokenId()
            .call({ from: sAccountAddress });
        console.log("Minted Token", mintCounter);

        return mintCounter;
    }

    async function priceCalculation() {
        $("#basic-addon3").html($("#customRange3").val());
        $("#basic-addon4").html(
            Number(await contract.methods.price().call({ from: sAccountAddress })) *
            $("#customRange3").val()
        );
    }

    async function referesh() {
        $("#progress-bar").attr("style", `width: ${(await getBalance()) / 100}%;`);
        $("#progress-bar").html(`${await getBalance()}`);
        $("#tokeninPool").html(`${await getBalance()}/10,000`);
    }
    referesh();

    // ===================================Owner Validation-------------------------------------------------------
    async function onlyOwnner() {
        let owner = await contract.methods.owner().call({ from: sAccountAddress });
        console.log("Cur", sAccountAddress);
        console.log("Owner", owner);
        if (sAccountAddress.toLowerCase() == owner.toLowerCase()) {
            // console.log(owner, "t");
            return true;
        } else {
            // console.log(owner, "f");
            return false;
        }
    }

    // -------------------------------------------------LogOut============================================================

    async function logOut() {
        $.ajax({
            method: "post",
            url: "/api/v1/auth/signOut",
            headers: {
                authorization: localStorage.getItem("authorization"),
                sign: sign.signature,
                hash: sign.hash,
            },
            success() {
                alert("Logged Out");
                $("#disconnect").hide();
                $("#connect").show();
                localStorage.removeItem("authorization");
                location.reload();
            },
            error() {
                console.log("Not authorized ");
            },
        });
    }

    // ======================================[[[DOM]]]==============================================
    $("#connect").click(async() => {
        // console.log("Recieved");
        await connect();
        sign = await getSign();
        await logIn(sign);
        referesh();
    });
    $("#set").click(() => {
        if (!contented) {
            alert("Connect Matamask First");
            return;
        }
        // connect();
        setPrice();
    });
    $("#buy").click(() => {
        if (!contented) {
            alert("Connect Matamask First");
            return;
        }
        // connect();
        buyTokens();
        referesh();
    });

    $("#disconnect").click(() => {
        logOut();
    });
    $("#customRange3").change(() => {
        priceCalculation();
    });

    $("#unit").change(async function() {
        let nAmount = Number($("#price").val());

        if (String($("#unit").val()) == "Eth") {
            console.log("Eth");
            nAmount = BigInt(nAmount);
            // =====================
            $("#price").val(`${await web3.utils.fromWei(String(nAmount))}`);
            // =====================
        } else if (String($("#unit").val()) == "Wei") {
            console.log("Wei");
            // =====================
            $("#price").val(`${await web3.utils.toWei(String(nAmount))}`);
            // =====================
        }
    });
});