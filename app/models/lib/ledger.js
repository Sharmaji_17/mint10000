const mongoose = require("mongoose");

const ledger = mongoose.Schema({
    Ref_id: {
        type: String,
        type: mongoose.Schema.Types.ObjectId,
        ref: "logged_ins",
        required: true,
        unique: false,
    },
    sTxHash: {
        type: String,
        required: true,
    },
    nQuantity: {
        type: Number,
        required: true,
    },
    nAmount: {
        type: mongoose.Decimal128,
        required: true,
    },
});

module.exports = mongoose.model("ledger", ledger);