const mongoose = require("mongoose");

const users = mongoose.Schema({
    sAddress: {
        type: String,
        required: true,
        ref: "ledger",
    },
    nTimeStamp: {
        type: Number,
        required: true,
    },
});

module.exports = mongoose.model("logged_In", users);