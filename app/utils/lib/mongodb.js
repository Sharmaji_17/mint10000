const mongoose = require("mongoose");

function MongoDB() {}

MongoDB.prototype.initialize = async function () {
  const ConnectionURL = "mongodb://127.0.0.1:27017/mint10000";
  mongoose.connect(ConnectionURL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  });
  const con = mongoose.connection;
  con.on("open", () => {
    console.log("connected");
  });
};

module.exports = new MongoDB();
