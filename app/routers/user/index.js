const { controllar } = require("./lib/controller");
const middlewere = require("./lib/middlewere");
const router = require("express").Router();

router.post("/onConnect", controllar.onConnect);
router.post("/onBuy", middlewere, controllar.onBuy);
router.post("/render", middlewere, controllar.renderUser);

module.exports = router;