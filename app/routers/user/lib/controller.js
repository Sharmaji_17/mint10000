const { user, ledger } = require("../../../models/index");
const { findOne } = require("../../../models/lib/User");
const controllar = {};

controllar.onConnect = async(req, res) => {
    let address = req.body.address;
    console.log(address);

    if (!address) {
        // res.status(400).send("Invalid Input");
        res.send(messages.required_field("Please Fill All Fields"));
    }

    const save = new user({
        sAddress: address,
        nTimeStamp: Date.now(),
    });
    save.save();
    console.log(save);
    // res.json(save);
    res.send(messages.successfully("User"));
    // let x = await user.find();
    // console.log(x);
};

controllar.onBuy = async(req, res) => {
    let sAddress = req.body.sAddress;
    let sTxHash = req.body.sTxHash;
    let nQuantity = req.body.nQuantity;
    let nAmount = req.body.nAmount;

    if (!sAddress || !sTxHash || !nQuantity || !nAmount) {
        // res.status(400).send("Invalid Input");
        console.log(
            "Not All valuea are set",
            sAddress,
            sTxHash,
            nQuantity,
            nAmount
        );
        res.status(400).send(messages.required_field("Please Fill All Fields"));
    }
    let data = await user.findOne({ sAddress: sAddress });

    const save = new ledger({
        Ref_id: data._id,
        sAddress: sAddress,
        sTxHash: sTxHash,
        nQuantity: nQuantity,
        nAmount: nAmount,
    });
    console.log(save);
    save.save();
    res.send(messages.successfully("Registerd"));

    let x = await user.findOne();
    console.log(x);
};

controllar.renderUser = async(req, res) => {
    let address = req.body.sAddress;
    console.log(address);

    if (!address) {
        res.status(400).send(messages.required_field(""));
    }

    let totalUser = await user.find({ sAddress: address });

    let render = await user.aggregate([{
            $match: {
                sAddress: address,
            },
        },
        {
            $project: {
                __v: 0,
            },
        },
        {
            $match: {
                $or: [{
                    sAddress: {
                        $regex: new RegExp(req.body.search.value, "i"),
                    },
                }, ],
            },
        },
        {
            $sort: {
                nTimeStamp: req.body.order[0].dir == "asc" ? 1 : -1,
            },
        },
        {
            $skip: Number(req.body.start),
        },
        {
            $limit: Number(req.body.length),
        },
    ]);
    let responseApi = {
        data: render,
        recordsFiltered: totalUser.length,
        recordsTotal: totalUser.length,
        draw: req.body.draw,
    };
    // console.log(render);
    console.log(responseApi);
    res.json(responseApi);
};

module.exports = { controllar };