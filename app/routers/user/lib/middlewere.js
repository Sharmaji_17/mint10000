const jwt = require("jsonwebtoken");
const Web3 = require("web3");
const Wutils = require("web3-utils");
const web3 = new Web3(new Web3.providers.HttpProvider("HTTP://127.0.0.1:7545"));
const util = require("ethereumjs-util");
const { user } = require("../../../models/index");
const middleware = async(req, res, next) => {
    let token = req.headers.authorization;
    console.log(req.header);

    console.log(token);
    // -----------------------Checking Session-------------------------------
    if (token == undefined) {
        return res
            .status(400)
            .send(messages.expired("[Please Log-in First],Session is"));
    }
    // ---------------------------decodeing signature--------------------
    // let msg = await Wutils.sha3("Hello Welcome to this amazing app");
    console.log(req.headers);
    let msg = util.toBuffer(req.headers.hash);
    resp = await util.fromRpcSig(req.headers.sign);
    const pubKey = await util.ecrecover(msg, resp.v, resp.r, resp.s);
    const addrBuf = await util.pubToAddress(pubKey);
    const address = await util.bufferToHex(addrBuf);
    console.log("Decoded", address);

    // -------------------------------Decoding Token---------------------

    let base64Url = token.split(".")[1];
    let base64 = JSON.parse(atob(base64Url));

    // ----------------------------------Finding in database--------------------------

    let userData = await user.findById(base64._id);
    console.log(base64);
    // console.log(userInfo);
    if (
        base64.sAddress != address ||
        base64.sAddress != req.session.sAddress ||
        base64._id != req.session._id ||
        userData.sAddress != req.session.sAddress
    ) {
        return res.send(messages.unauthorized("Sign Doesnt Match"));
    } else {
        console.log("Success");
        next();
    }
};

module.exports = middleware;