const controllers = {};
const { user } = require("../../../models/index");
const jwt = require("jsonwebtoken");

controllers.signIn = async(req, res) => {
    if (req.body.sAddress == undefined || !req.body.sAddress) {
        return res.send(messages.not_found("Signature not found"));
    }

    var userInfo = await user.findOne({ sAddress: req.body.sAddress });

    const token = await jwt.sign({
            sAddress: req.body.sAddress,
            _id: userInfo._id,
        },
        "key", { expiresIn: "1h" }
    );

    req.session["-Token-"] = token;
    req.session["sAddress"] = userInfo.sAddress;
    req.session["_id"] = userInfo._id;

    return res.json({
        Status: messages.successfully("Signed-In"),
        token: token,
    });
};

controllers.signout = async(req, res) => {
    req.session.destroy();
    res.send(messages.successfully("Loged-Out"));
};

module.exports = controllers;