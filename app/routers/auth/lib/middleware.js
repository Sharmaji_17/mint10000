const jwt = require("jsonwebtoken");
const { user } = require("../../../models/index");
const Wutils = require("web3-utils");
const util = require("ethereumjs-util");

const middleware = async(req, res, next) => {
    // let token = req.session.token;
    let token = req.headers.authorization;

    // -----------------------Checking Session-------------------------------
    if (token == undefined) {
        return res
            .status(400)
            .send(messages.expired("[Please Log-in First],Session is"));
    }

    // -------------------------------Decoding Token---------------------

    let base64Url = token.split(".")[1];
    let base64 = JSON.parse(atob(base64Url));
    // console.log(base64.email);

    // ---------------------------decodeing signature--------------------
    console.log(req.headers.sign);
    console.log(req.headers.hash);
    // console.log(JSON.parse(req.headers.sign));

    let msg = util.toBuffer(req.headers.hash);

    resp = await util.fromRpcSig(req.headers.sign);

    const pubKey = await util.ecrecover(msg, resp.v, resp.r, resp.s);
    const addrBuf = await util.pubToAddress(pubKey);
    const address = await util.bufferToHex(addrBuf);
    console.log("Decoded", address);
    // ----------------------------------Finding in database--------------------------

    let userData = await user.findById(base64._id);

    // console.log(userInfo);
    if (
        (base64.sAddress != address || base64.sAddress != req.session.sAddress) |
        (base64._id != req.session._id) |
        (userData.sAddress != req.session.sAddress)
    ) {
        console.log(` (${base64.sAddress} != ${address} || ${base64.sAddress} != ${req.session.sAddress}) |
       (${base64._id} != ${req.session._id}) |
       (${userData.sAddress} != ${req.session.sAddress})`);
        console.log("Not Success");
        return res.status(400).send(messages.unauthorized("Sign Doesnt Match"));
    } else {
        console.log("Success");
        next();
    }
};

module.exports = middleware;