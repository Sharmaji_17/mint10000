const router = require("express").Router();
const controllers = require("./lib/controllers");
const middlewere = require("./lib/middleware");
// router.post("/isEmailReceived", controllers.isEmailReceived); // This is for an example
router.post("/signIn", controllers.signIn); // This is for an example
router.post("/signOut", middlewere, controllers.signout); // This is for an example

module.exports = router;