const router = require("express").Router();
const uiRoute = require("./render");
const userRoute = require("./user/index");
const auth = require("./auth/index");

router.use("/", uiRoute);
router.use("/user", userRoute);
router.use("/auth", auth);

module.exports = router;