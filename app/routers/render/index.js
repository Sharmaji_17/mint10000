const express = require("express");
const router = express.Router();
const { oRenderController } = require("./lib/controller");

router.get("/", oRenderController.render);
router.get("/table", oRenderController.tableRender);
// router.get("/", (req, res) => {});

module.exports = router;