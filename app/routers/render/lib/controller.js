const oRenderController = {};

oRenderController.render = (req, res) => {
    res.render("index");
};
oRenderController.tableRender = (req, res) => {
    res.render("table");
};

module.exports = { oRenderController };